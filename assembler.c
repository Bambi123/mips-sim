// COMP1521 20T3 Assignment 1: mips_sim -- a MIPS simulator

//This is the assembler library for all used functions in mips_sim.c

// Thomas French z5206283
// 1st of November, 2020
// A MIPS Simulator

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

uint32_t add(uint32_t s, uint32_t t){return s + t;}

uint32_t sub(uint32_t s, uint32_t t){return s - t;}

uint32_t slt(uint32_t s, uint32_t t){ return s < t;}

uint32_t mul(uint32_t s, uint32_t t){return s * t;}

uint32_t beq(uint32_t s, uint32_t t){return (s == t);}

uint32_t bne(uint32_t s, uint32_t t){return (s != t);}

uint32_t addi(uint32_t s, int16_t I){return s + I;}

uint32_t ori(uint32_t s, int16_t I){return (s | I);}

uint32_t lui(int16_t I){return I << 16;}

void syscall(uint32_t v0, uint32_t a0, int trace_mode){

    if (v0 == 1){ // print integer
        if (trace_mode){printf("<<< ");}
        printf("%d", a0);
    } else if (v0 == 11){ // print character
        if (trace_mode){printf("<<< ");}
        printf("%c", a0);
    } else if (v0 == 10){ // exit program
        exit(EXIT_SUCCESS);
    } else { // unknwon system call
        printf("Unknown system call: %d\n", v0);
        exit(EXIT_FAILURE);
    }
    if (trace_mode){printf("\n");}
}
