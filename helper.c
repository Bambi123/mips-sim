// COMP1521 20T3 Assignment 1: mips_sim -- a MIPS simulator

//This is the helper library for all used functions in mips_sim.c

// Thomas French z5206283
// 1st of November, 2020
// A MIPS Simulator

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#define NO_OF_REGISTERS 32

//Prints any 32 bit input in binary
void print_to_binary(uint32_t n){

    uint32_t comp = 0x80000000; // 1 bit at the start
    for (int i = 0; i < 32; i++){
        if ( (n & comp) == 0){
            printf("0");
        } else {
            printf("1");
        }
        comp = comp >> 1;
    }
    printf("\n");
}

int check_pc(int pc, int n){

    //return 0 if pc is not okay
    //return 1 if pc is okay
    int new = pc + 1;
    if (new >= 0 && new < n){
        return 1;
    }
    return 0;
}

void redefine_zero_register(uint32_t registers[NO_OF_REGISTERS]){

    registers[0] = 0;

}
