//Thomas French z5206283
// COMP1521 20T3 Assignment 1: mips_sim -- a MIPS simulator

//This is the assembler library for all used functions in mips_sim.c

/*
The add function takes two unsigned 32 bit integers and returns the sum.
The physical register values ar given as arguments and the return register 
is filled in the higher function.
*/
uint32_t add(uint32_t s, uint32_t t);

/*
The sub function takes two unsigned 32 bit integers and returns the difference.
The physical register values ar given as arguments and the return register 
is filled in the higher function.
*/
uint32_t sub(uint32_t s, uint32_t t);

/*
The slt function takes two unsigned 32 bit integers and returns argument s left
shifted by argument t.
The physical register values ar given as arguments and the return register 
is filled in the higher function.
*/
uint32_t slt(uint32_t s, uint32_t t);

/*
The mul function takes two unsigned 32 bit integers and returns the product.
The physical register values ar given as arguments and the return register 
is filled in the higher function.
*/
uint32_t mul(uint32_t s, uint32_t t);

/*
The beq function takes two unsigned 32 bit integers and returns a 1 if
the two arguments are equivalent or a 0 if they are not.
The resuly is returned and placed into the argument register in the higher
function.
*/
uint32_t beq(uint32_t s, uint32_t t);

/*
The bne function takes two unsigned 32 bit integers and returns a 0 if
the two arguments are equivalent or a 1 if they are not.
The resuly is returned and placed into the argument register in the higher
function.
*/
uint32_t bne(uint32_t s, uint32_t t);

/*
The addi function takes a 32 bit unsigned integer and a signed 16 bit integer
and returns the sum of the two.  The sum is returned to the argument register
in the higher function.
*/
uint32_t addi(uint32_t s, int16_t I);

/*
The ori function takes a 32 bit unsigned integer and a signed 16 bit integer
and returns the bitwise OR if the two.  The bitwise OR is returned to the
argument register in the higher function.
*/
uint32_t ori(uint32_t s, int16_t I);

/*
The lui function takes a 16 bit signed integer and returns the value bitwise
left shifted by 16.  The result is returned to the argument register in the
higher function.  
*/
uint32_t lui(int16_t I);

/*
The syscall function takes the register v0, register a0 and the tracemode 
indicator and prints out accordingly th output from syscall.
The function is a void return, only prints out required output with no
return.  
*/
void syscall(uint32_t v0, uint32_t a0, int trace_mode);




