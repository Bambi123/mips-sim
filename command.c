// COMP1521 20T3 Assignment 1: mips_sim -- a MIPS simulator

//This is the command library for all used functions in mips_sim.c

// Thomas French z5206283
// 1st of November, 2020
// A MIPS Simulator

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "assembler.h"
#include "helper.h"

#define NO_OF_REGISTERS 32

//32 bit instruction extract codes for commands

#define ACTION_EXTRACT_I    0b11111100000000000000000000000000
#define ACTION_EXTRACT      0b11111100000000000000011111111111

#define S_EXTRACT           0b00000011111000000000000000000000
#define T_EXTRACT           0b00000000000111110000000000000000
#define D_EXTRACT           0b00000000000000001111100000000000
#define I_EXTRACT           0b00000000000000001111111111111111

#define ADD                 0b00000000000000000000000000100000
#define SUB                 0b00000000000000000000000000100010
#define SLT                 0b00000000000000000000000000101010
#define MUL                 0b01110000000000000000000000000010

#define BEQ                 0b00010000000000000000000000000000
#define BNE                 0b00010100000000000000000000000000
#define ADDI                0b00100000000000000000000000000000
#define ORI                 0b00110100000000000000000000000000
#define LUI                 0b00111100000000000000000000000000

#define SYSCALL             0b00000000000000000000000000001100

void define_registers(uint32_t registers[NO_OF_REGISTERS]){

    for (int i = 0; i < NO_OF_REGISTERS; i++){
        registers[i] = 0;
    }

}

void execute_command(uint32_t command, uint32_t registers[NO_OF_REGISTERS],
                    int* pc, int trace_mode, int n_instructions){

    if (trace_mode){printf("%d: 0x%08X ", *pc, command);}

    //we first have to extract the 16 signed bit actions and test
    uint32_t action = (command & ACTION_EXTRACT_I);

    uint32_t s = (command & S_EXTRACT);
    uint32_t t = (command & T_EXTRACT);
    uint32_t d = (command & D_EXTRACT);
    int16_t  I = (command & I_EXTRACT);

    s = s >> 21;
    t = t >> 16;
    d = d >> 11;
    I = I >> 0;

    switch(action){

        case BEQ:
            if (trace_mode){printf("beq  $%d, $%d, %d\n", s, t, I);}
            if (beq(registers[s], registers[t])){

                *pc += (I - 1);
                if (trace_mode){printf(">>> branch taken to PC = %d\n", *pc + 1);}
                if (!check_pc(*pc, n_instructions)){
                    printf("Illegal branch to address before instructions: PC = %d\n", *pc + 1);
                    exit(EXIT_FAILURE);
                }

            } else {
                if (trace_mode){printf(">>> branch not taken\n");}
            }
            redefine_zero_register(registers);
            return;
        case BNE:
            if (trace_mode){printf("bne  $%d, $%d, %d\n", s, t, I);}
            if (bne(registers[s], registers[t])){

                *pc += (I - 1);
                if (trace_mode){printf(">>> branch taken to PC = %d\n", *pc + 1);}
                if (!check_pc(*pc, n_instructions)){
                    printf("Illegal branch to address before instructions: PC = %d\n", *pc + 1);
                    exit(EXIT_FAILURE);
                }
                
            } else {
                if (trace_mode){printf(">>> branch not taken\n");}
            }
            redefine_zero_register(registers);
            return;
        case ADDI:
            registers[t] = addi(registers[s], I);
            if (trace_mode){
                printf("addi $%d, $%d, %d\n", t, s, I);
                printf(">>> $%d = %d\n", t, registers[t]);
            }
            redefine_zero_register(registers);
            return;
        case ORI:
            registers[t] = ori(registers[s], I);
            if (trace_mode){
                printf("ori  $%d, $%d, %d\n", t, s, I);
                printf(">>> $%d = %d\n", t, registers[t]);
            }
            redefine_zero_register(registers);
            return;
        case LUI:
            registers[t] = lui(I);
            if (trace_mode){
                printf("lui  $%d, %d\n", t, I);
                printf(">>> $%d = %d\n", t, registers[t]);
            }
            redefine_zero_register(registers);
            return;
    }

    // if none of these hit, then the case must be a more general command

    action = (command & ACTION_EXTRACT);

    switch(action){

        case ADD:
            registers[d] = add(registers[s], registers[t]);
            if (trace_mode){
                printf("add  $%d, $%d, $%d\n", d, s, t);
                printf(">>> $%d = %d\n", d, registers[d]);
            }
            redefine_zero_register(registers);
            return;
        case SUB:
            registers[d] = sub(registers[s], registers[t]);
            if (trace_mode){
                printf("sub  $%d, $%d, $%d\n", d, s, t);
                printf(">>> $%d = %d\n", d, registers[d]);
            }
            redefine_zero_register(registers);
            return;
        case SLT:
            registers[d] = slt(registers[s], registers[t]);
            if (trace_mode){
                printf("slt  $%d, $%d, $%d\n", d, s, t);
                printf(">>> $%d = %d\n", d, registers[d]);
            }
            redefine_zero_register(registers);
            return;
        case MUL:
            registers[d] = mul(registers[s], registers[t]);
            if (trace_mode){
                printf("mul  $%d, $%d, $%d\n", d, s, t);
                printf(">>> $%d = %d\n", d, registers[d]);
            }
            redefine_zero_register(registers);
            return;
        case SYSCALL:
            if (trace_mode){
                printf("syscall\n");
                printf(">>> syscall %d\n", registers[2]);
            }
            syscall(registers[2], registers[4], trace_mode);
            redefine_zero_register(registers);
            return;
    }

    //if we get to here, then we have an invalid instruction code
    printf("invalid instruction code\n");
    exit(EXIT_FAILURE);

}


