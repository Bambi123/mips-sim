//Thomas French z5206283
// COMP1521 20T3 Assignment 1: mips_sim -- a MIPS simulator

//This is the helper library for all used functions in mips_sim.c

#define NO_OF_REGISTERS 32

/*
The print_to_binary function takes any 32 bit unsigned integer and prints
out its binary form.  This is only used when debugging the program when
it functions incorrectly and is not used anywhere in the submitted functionality.
*/
void print_to_binary(uint32_t n);

/*
The check_pc function, meaning check program counter, tests to see if the
current program counter is legal.  If the program counter is not an accessible 
section of the MIPS program than th program returns 0.  Otherwise, this function
returns 1.
*/
int check_pc(int pc, int n);

/*
Th redefine_zero_rgister function redefines $0 at the end of every line of MIPS
code.  This is done with accordance of assignment spec and must always be kept
to zero despite changes.  
*/
void redefine_zero_register(uint32_t registers[NO_OF_REGISTERS]);
