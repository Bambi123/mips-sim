//Thomas French z5206283
// COMP1521 20T3 Assignment 1: mips_sim -- a MIPS simulator

//This is the helper library for all used functions in mips_sim.c


#define NO_OF_REGISTERS 32


/*
0 - always zero
1 - reserved for assembler
2 - $v0
4 - $a0
26 - reserved for operating system
27 - reserved for operating system
28, 29, 30, 31 - funky pointers
*/

/*
The define_registers takes an undefined array of MIPS registers and converts
all registers to zero.  Since the registers array exists outside of the function,
we need not return any array.  
*/
void define_registers(uint32_t registers[NO_OF_REGISTERS]);

/*
The execute_command function takes individual commands from the execute_instruction
function in mips_sim.c and breaks down all the information into command codes, register
values, 16 signed bit integers.  The function determins which actions need to be
completed and the appropriate output for the MIPS Simulator program.  
*/
void execute_command(uint32_t command, uint32_t registers[NO_OF_REGISTERS],
                     int *pc, int trace_mode, int n_instructions);



